const startbtn = document.getElementById('start-btn');
startbtn.addEventListener('click', start);

const nxtBtn = document.getElementById('next-btn');

const questionContainerElement = document.getElementById('question-container');
const quesEle = document.getElementById('question');
const ansBtnEle = document.getElementById('answer-btn')

let shuffledQsn, currQsnIndex;

nxtBtn.addEventListener('click', () =>{
    currQsnIndex++;
    setNextQuestion();
})

function start(){
    console.log("Start");
    startbtn.classList.add('hide');
    shuffledQsn = questions.sort(() => {
        Math.random() - 0.5
    });
    currQsnIndex = 0;
    questionContainerElement.classList.remove('hide');
    setNextQuestion()
}

function setNextQuestion(){
    resetState();
    showQuestion(shuffledQsn[currQsnIndex])
}

function showQuestion(question){
    quesEle.innerText = question.question;
    question.answers.forEach(answer => {
        const button = document.createElement('button');
        button.innerText = answer.text;
        button.classList.add('btn');
        if(answer.correct){
            button.dataset.correct = answer.correct;
        }
        button.addEventListener('click', selectAnswer);
        ansBtnEle.appendChild(button);
    });
}

function resetState(){
    clearStatusClass(document.body)
    nxtBtn.classList.add('hide');
    while(ansBtnEle.firstChild){
        ansBtnEle.removeChild(ansBtnEle.firstChild);
    }
}

function selectAnswer(e){
    const selectedBtn = e.target;
    const correct = selectedBtn.dataset.correct
    setStatusClass(document.body, correct)
    Array.from(ansBtnEle.children).forEach(button =>{
        setStatusClass(button, button.dataset.correct);
    });
    if(shuffledQsn.length > currQsnIndex + 1){
        nxtBtn.classList.remove('hide');
    }
    else{
        startbtn.innerText = 'Restart';
        startbtn.classList.remove('hide');
    }
}

function setStatusClass(ele, correct){
    clearStatusClass(ele)
    if(correct){
        ele.classList.add('correct');
    }else{
        ele.classList.add('wrong');
    }
}

function clearStatusClass(ele){
    ele.classList.remove('correct');
    ele.classList.remove('wrong');
}

const questions = [
    {
        question: 'Who won the 2019 world cup?',
        answers: [
            {
                text: 'England',
                correct: true
            },
            {
                text: 'Australia',
                correct: false
            },
            {
                text: 'India',
                correct: false
            },
            {
                text: 'New Zealand',
                correct: false
            }
        ]
    },
    {
        question: 'Who is the runners up of 2019 world cup?',
        answers: [
            {
                text: 'England',
                correct: false
            },
            {
                text: 'Australia',
                correct: false
            },
            {
                text: 'India',
                correct: false
            },
            {
                text: 'New Zealand',
                correct: true
            }
        ]
    }
]